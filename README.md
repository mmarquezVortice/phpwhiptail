# whiptail
a php cli wrapper for linux [whiptail](http://linux.die.net/man/1/whiptail)

## Installation
The recommended way to install is [through composer](http://getcomposer.org).

```
{
    "require": {
        "Vortice/whiptail": "@stable"

    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/mmarquezVortice/phpwhiptail.git"
        }
    ],
    "minimum-stability": "dev"
}
```

## Usage
```php

use WhipTail\Controller as WhipTail;

if (WhipTail::isAvailable()) {

    $whipTail = new WhipTail(option: WindowTypes::PasswordBox, message: 'new password');
    $whipTail
        ->option(BoxOptions::Title, 'Password')
        ->run();
}

```

## Available Boxes

The available boxes can be checked in WindowTypes. 

```
MessageBox
PasswordBox
InfoBox
GaugeBox
QuestionBox
TextBox
InputBox
MenuBox
RadioBox
CheckBox
```
To see demo of all options look at tests folder.

## Box options

Box options can be set by method option, first argument is the option name and second the argument.

options:
```
	Clear				            clear screen on exit
	DefaultNoButton		            default no button
	DefaultText        <text>		set default string
	FullButtons		                use full buttons
	NoCancelButton		            no cancel button
	YesButtonText      <text>		set text of yes button
	NoButtonText       <text>		set text of no button
	OkButtonText       <text>		set text of ok button
	CancelButtonText   <text>		set text of cancel button
	TagsOnly			            display tags only
	SeparateOutput		            output one line at a time
	OutputToFD         <fd>		    output to fd, not stdout
	Title             <title>		display title
	DisplayBackTitle  <backtitle>	display backtitle
	ScrollText			            force vertical scrollbars
	TopLeft			                put window in top-left corner
```


