<?php
declare(strict_types=1);

namespace Vortice\WhipTail\Helpers;

enum WindowTypes: string
{
    case MessageBox = 'msgbox';
    case PasswordBox = 'passwordbox';
    case InfoBox = 'infobox';
    case GaugeBox = 'gauge';
    case QuestionBox = 'yesno';
    case TextBox = 'textbox';
    case InputBox = 'inputbox';
    case MenuBox = 'menu';
    case RadioBox = 'radiolist';
    case CheckBox = 'checklist';
}