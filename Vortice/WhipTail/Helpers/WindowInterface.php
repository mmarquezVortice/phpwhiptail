<?php
declare(strict_types=1);

namespace Vortice\WhipTail\Helpers;

interface WindowInterface
{
    public function __construct(int $width = 70, int $height = 20, string $message = '');
}