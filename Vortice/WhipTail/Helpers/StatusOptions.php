<?php
declare(strict_types=1);

namespace Vortice\WhipTail\Helpers;

enum StatusOptions: string
{
    case ON = 'ON';
    case OFF = 'OFF';
}