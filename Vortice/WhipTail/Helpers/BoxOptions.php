<?php
declare(strict_types=1);

namespace Vortice\WhipTail\Helpers;

enum BoxOptions: string
{
    case Clear = 'clear';
    case DefaultNoButton = 'defaultno';
    case DefaultText = 'default-item';
    case FullButtons = 'fb';
    case NoCancelButton = 'nocancel';
    case YesButtonText = 'yes-button';
    case NoButtonText = 'no-button';
    case OkButtonText = 'ok-button';
    case CancelButtonText = 'cancel-button';
    case TagsOnly = 'noitem';
    case SeparateOutput = 'separate-output';
    case OutputToFD = 'output-fd';
    case Title = 'title';
    case DisplayBackTitle = 'backtitle';
    case ScrollText = 'scrolltext';
    case TopLeft = 'topleft';

    public static function needString(BoxOptions $option) : bool
    {
        return match ($option) {
            self::DefaultText,
            self::YesButtonText,
            self::NoButtonText,
            self::OkButtonText,
            self::CancelButtonText,
            self::OutputToFD,
            self::Title,
            self::DisplayBackTitle => true,
            default => false
        };
    }
}