<?php
declare(strict_types=1);

/**
 * @author    Moisés Márquez <moisesmarquez@vorticesoft.com>
 * @author    Philip Bergman <pbergman@live.nl>
 *
 * Thanks to the previous work of Philip Bergman
 */

namespace Vortice\WhipTail;

use Vortice\WhipTail\Helpers\BoxOptions;
use Vortice\WhipTail\Helpers\WindowInterface;
use Vortice\WhipTail\Helpers\WindowTypes;
use Vortice\WhipTail\Options\BaseOption;

class WhipTail
{
    private WindowInterface&BaseOption $command;
    private ?string $output = null;
    private int $exitCode = 0;
    private static ?string $version = null;

    /**
     * will return dialog version, if installed
     */
    public static function getVersion(): ?string
    {
        if (is_null(static::$version)) {
            static::$version = shell_exec('dpkg-query -W --showformat=\'${Version}\' whiptail 2> /dev/null');
        }

        return static::$version;
    }

    /**
     * will return true is dialog is available
     */
    public static function isAvailable(): bool
    {
        return (bool) WhipTail::getVersion();
    }

    public function __construct(
        WindowTypes $option,
        int $width = 70,
        int $height = 20,
        string $message = ''
    ) {
        if (WhipTail::isAvailable() === false){
            throw new \ErrorException('Could not find package "whiptail"');
        }

        $className = __NAMESPACE__ . '\\Options\\' . $option->name;

        if (class_exists($className)) {
            $this->command = new $className(width: $width, height: $height, message: $message);
        }
    }

    /**
     * Returns the constructed command/box
     *
     * @return WindowInterface
     */
    public function box(): WindowInterface
    {
        return $this->command;
    }

    public function option(BoxOptions $option, string|bool $value): self
    {
        $this->command?->option($option, $value);
        return $this;
    }

    /**
     * runs the build command, STDOUT is used to passthru the
     * dialog display, STDIN to passthru input and STDOUT set
     * as pipe here, will contain the error message and in
     * case with whiptail will also contain the return message
     *
     * @throws \Exception
     */
    public function run()
    {
        $isGauge =  ($this->command->name === 'gauge');

       if ($isGauge) {
           $descriptorSpec = array(
               array("pipe", "r"),
               STDOUT,
               array("pipe", "w"),
           );
       } else {
           $descriptorSpec = array(
               STDIN,
               STDOUT,
               array("pipe", "w"),
           );
       }

        /**
         * @todo Extract this
         */
       if ($this->command->name !== 'infobox') {
           $fullCommand = sprintf("whiptail %s", $this->command->getArguments());
       } else {
           $fullCommand = sprintf("TERM=ansi whiptail %s", $this->command->getArguments());
       }

        // open the child
        $proc = proc_open($fullCommand, $descriptorSpec, $pipes, getcwd());

        // set all streams to non blocking mode
        foreach ($descriptorSpec as $key => $ds) {
            if (is_array($ds)) {
                stream_set_blocking($pipes[$key], false);
            } else {
                stream_set_blocking($ds, false);
            }
        };

        if (is_resource($proc)) {
            // Loop while running
            while(true) {
                if (false !== $status = proc_get_status($proc)){

                    if($status['running'] === false) {

                        $this->output   = stream_get_contents($pipes[2]);
                        $this->exitCode = $status['exitcode'];
                        fclose($pipes[2]);
                        break;

                    } else {
                        if ($this->command->name === 'gauge') {
                            $this->command->process($pipes[0]);
                        }
                    }
                } else {
                    throw new \Exception('Could not get proc status');
                }
            }
        } else {
            throw new \Exception('Cannot execute child process');
        }

    }

    public function getExitCode(): int
    {
        return $this->exitCode;
    }

    /**
     * Check if the output of the Box is true (no error = 0)
     *
     * @return bool
     */
    public function isTrue(): bool
    {
        return $this->exitCode === 0;
    }

    /**
     * Check if the output of the Box is not true (error != 0)
     *
     * @return bool
     */
    public function isFalse(): bool
    {
        return $this->exitCode !== 0;
    }

    /**
     * Exit status is 0 if whiptail is exited by pressing the Yes or OK button,
     * and 1 if the No or Cancel button is pressed. Otherwise, if errors occur
     * inside whiptail or whiptail is exited by pressing the ESC key, the exit
     * status is -1.
     *
     * @return bool
     */
    public function isSuccess(): bool
    {
        return ($this->exitCode === 1 || $this->exitCode === 0);
    }

    /**
     * return whiptails output result
     */
    public function getResult(): array|string
    {
        if ($this->command?->getOption(BoxOptions::SeparateOutput)) {
            return array_filter(explode("\n", $this->output));
        }
        return $this->output;
    }
}
