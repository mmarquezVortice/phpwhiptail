<?php
/**
 * @author    Philip Bergman <Vortice@live.nl>
 * @copyright Philip Bergman
 */

namespace Vortice\WhipTail\Options;

use Vortice\WhipTail\Helpers\BoxOptions;

/**
 * Class BaseOption
 *
 * Helper class with some basic functions
 *
 * @package Vortice\WhipTail\Options
 */
class BaseOption
{
    protected \WeakMap $options;

    public function __construct(
      public readonly string $name = '',
      public readonly int $width = 70,
      public readonly int $height = 20,
      public readonly string $message = ''
    ) {
        $this->options = new \WeakMap;
    }

    public function option(BoxOptions $option, bool|string $value = true): self
    {
        $needString = BoxOptions::needString($option);
        if (($needString && !is_string($value))
            || (!$needString && !is_bool($value))
        ) {
            throw new \InvalidArgumentException(sprintf('Option "%s" needs a valid value', $option->value));
        }
        $this->options->offsetSet($option, $value);
        return $this;
    }

    /**
     * Get the value of the option or null
     *
     * @param BoxOptions $option
     * @return bool|string|null
     */
    public function getOption(BoxOptions $option): null|bool|string
    {
        if ($this->options->offsetExists($option)) {
            return $this->options->offsetGet($option);
        } else {
            return null;
        }
    }

    public function optionsToString(): string
    {
        $toRet = '';
        /**
         * @var BoxOptions $option
         * @var mixed $value
         */
        foreach ($this->options as $option => $value) {
            if (is_string($value)) {
                $toRet .= sprintf(' --%s "%s"', $option->value, $value);
            } else if ($value === true) {
                $toRet .= sprintf(' --%s', $option->value);
            }
        }
        return $toRet;
    }

    /**
     * Method is called to get the arguments
     */
    public function getArguments()
    {
        if (empty($this->name)) {
            throw new \Exception('Need to set a option name first!');
        }

        return sprintf("--%s %s \"%s\" %s %s", $this->name, $this->optionsToString(), $this->message, $this->height,  $this->width);
    }
}