<?php
/**
 * @author    Philip Bergman <Vortice@live.nl>
 * @copyright Philip Bergman
 */

namespace Vortice\WhipTail\Options;

use Vortice\WhipTail\Helpers\WindowInterface;
use Vortice\WhipTail\Helpers\WindowTypes;

/**
 * Class MessageBox
 *
 * A message box is very similar to a yes/no box. The only difference between a message
 * box and a yes/no box is that a message box has only a single OK button. You can use
 * this dialog box to display any message you like. After reading the message, the user
 * can press the ENTER key so that whiptail will exit and the calling shell script can
 * continue its operation.
 *
 * @package Vortice\WhipTail\Options
 */
class MessageBox extends BaseOption implements WindowInterface
{
    public function __construct(int $width = 70, int $height = 20, string $message = '')
    {
        parent::__construct(
            name: WindowTypes::MessageBox->value, width: $width, height: $height, message: $message
        );
    }
}