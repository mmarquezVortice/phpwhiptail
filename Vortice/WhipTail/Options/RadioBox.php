<?php
/**
 * @author    Philip Bergman <Vortice@live.nl>
 * @copyright Philip Bergman
 */

namespace Vortice\WhipTail\Options;

use Vortice\WhipTail\Helpers\StatusOptions;
use Vortice\WhipTail\Helpers\WindowInterface;
use Vortice\WhipTail\Helpers\WindowTypes;

/**
 * Class RadioList
 *
 * A radiolist box is similar to a menu box. The only difference is that you can indicate
 * which entry is currently selected, by setting its status to on.
 *
 * @package Vortice\WhipTail\Options
 */
class RadioBox extends BaseOption implements WindowInterface
{
    private array $list = [];

    public function __construct(int $width = 70, int $height = 20, string $message = '')
    {
        parent::__construct(
            name: WindowTypes::RadioBox->value, width: $width, height: $height, message: $message
        );
    }

    /**
     * add item to list
     *
     * @param string $id
     * @param string $description
     * @param StatusOptions $status
     *
     */
    public function addToList(string $id, string $description, StatusOptions $status)
    {
        $this->list[$id] = [$description, $status->value];
    }

    /**
     * give al whole list at once
     *
     * should be like
     *
     * array(
     *  array("ITEM1", "Some desc", "ON"),
     *  array("ITEM2", "Some other desc", "OFF"),
     *  array("ITEM3", "Some other desc", "OFF"),
     * )
     *
     * @param array $stack
     *
     * @throws \InvalidArgumentException
     */
    public function setList(array $stack)
    {
        foreach($stack as $item) {

            if (count($item) === 3) {
                $this->addToList($item[0], $item[1], $item[2]);
            } else {
                throw new \InvalidArgumentException(sprintf("Given list item is invalid. Expected format: %s given: %s\n", print_r(array("ITEM1", "Some desc", "ON"), true), print_r($item, true)));            }

        }
    }


    /**
     * @return string
     */
    public function getListString()
    {
        $listedString = '';
        foreach ($this->list as $id => $data) {
            $listedString .= sprintf(' "%s" "%s" %s', $id, $data[0], $data[1]);
        }
        return $listedString;
    }


    /**
     * Method is called to get the full command back
     */
    public function getArguments()
    {
        return sprintf(
            "--%s %s \"%s\" %s %s %s %s",
            $this->name,
            $this->optionsToString(),
            $this->message,
            $this->height,
            $this->width,
            $this->height - 8,
            $this->getListString()
        );
    }

}