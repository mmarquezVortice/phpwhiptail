<?php
/**
 * @author    Philip Bergman <Vortice@live.nl>
 * @copyright Philip Bergman
 */

namespace Vortice\WhipTail\Options;

use Vortice\WhipTail\Helpers\BoxOptions;
use Vortice\WhipTail\Helpers\StatusOptions;
use Vortice\WhipTail\Helpers\WindowInterface;
use Vortice\WhipTail\Helpers\WindowTypes;

/**
 * Class CheckBox
 *
 * A checklist box is similar to a menu box in that there are multiple entries presented
 * in the form of a menu. You can select and deselect items using the SPACE key. The
 * initial on/off state of each entry is specified by status. On exit, a list of the tag
 * strings of those entries that are turned on will be printed on stderr
 *
 * @package Vortice\WhipTail\Options
 */
class CheckBox extends BaseOption implements WindowInterface
{
    private array $list = [];

    public function __construct(int $width = 70, int $height = 20, string $message = '')
    {
        parent::__construct(
            name: WindowTypes::CheckBox->value, width: $width, height: $height, message: $message
        );
        parent::option(BoxOptions::SeparateOutput, true);
    }

    /**
     * add item to list
     *
     * @param string $id
     * @param string $description
     * @param StatusOptions $status
     *
     */
    public function addToList(string $id, string $description, StatusOptions $status)
    {
        $this->list[$id] = [$description, $status->value];
    }

    /**
     * add a array stack as list
     *
     * pattern should be like
     *
     * array(
     *  array("ITEM1", "Some desc", "ON"),
     *  array("ITEM2", "Some other desc", "OFF"),
     *  array("ITEM3", "Some other desc", "OFF"),
     * )
     *
     * @param array $stack
     *
     * @throws \InvalidArgumentException
     */
    public function setList(array $stack)
    {
        foreach($stack as $item) {
            if (count($item) === 3) {
                $this->addToList($item[0], $item[1], $item[2]);
            } else {
                throw new \InvalidArgumentException(sprintf("Given list item is invalid. Expected format: %s given: %s\n", print_r(array("ITEM1", "Some desc", "ON"), true), print_r($item, true)));
            }
        }
    }


    /**
     * @return string
     */
    public function getListString()
    {
        $string = '';

        foreach ($this->list as $id => $data) {
            $string .= sprintf(' "%s" "%s" %s', $id, $data[0], $data[1]);
        }

        return $string;
    }


    /**
     * Method is called to get the full command back
     */
    public function getArguments()
    {
        return sprintf(
            "--%s %s \"%s\" %s %s %s %s",
            $this->name,
            $this->optionsToString(),
            $this->message,
            $this->height,
            $this->width,
            $this->height - 8,
            $this->getListString()
        );
    }

}