<?php
/**
 * @author    Philip Bergman <Vortice@live.nl>
 * @copyright Philip Bergman
 */

namespace Vortice\WhipTail\Options;

use Vortice\WhipTail\Helpers\WindowInterface;
use Vortice\WhipTail\Helpers\WindowTypes;

/**
 * Class Menu
 *
 * As its name suggests, a menu box is a dialog box that can be used to present a
 * list of choices in the form of a menu for the user to choose. Each menu entry
 * consists of a tag string and an item string. The tag gives the entry a name to
 * distinguish it from the other entries in the menu. The item is a short description
 * of the option that the entry represents. The user can move between the menu
 * entries by pressing the UP/DOWN keys, the first letter of the tag as a hot-key.
 * There are menu-height entries displayed in the menu at one time, but the menu will
 * be scrolled if there are more entries than that. When whiptail exits, the tag of
 * the chosen menu entry will be printed on stderr.
 *
 * @package Vortice\WhipTail\Options
 */
class MenuBox extends BaseOption implements WindowInterface
{
    private array $list = [];

    public function __construct(int $width = 70, int $height = 20, string $message = '')
    {
        parent::__construct(
            name: WindowTypes::MenuBox->value, width: $width, height: $height, message: $message
        );
    }

    /**
     * add item to list
     *
     * @param  string $id
     * @param  string $description
     *
     */
    public function addToList(string $id, string $description)
    {

        $this->list[$id] = [$description];
    }

    /**
     * give al whole list at once
     *
     * should be like
     *
     * array(
     *  array("ITEM1", "Some desc"),
     *  array("ITEM2", "Some other desc"),
     *  array("ITEM3", "Some other desc"),
     * )
     *
     * @param array $stack
     *
     * @throws \InvalidArgumentException
     */
    public function setList(array $stack)
    {
        foreach($stack as $item) {
            if (count($item) === 2) {
                $this->addToList($item[0], $item[1]);
            } else {
                throw new \InvalidArgumentException(sprintf("Given list item is invalid. Expected format: %s given: %s\n", print_r(array("ITEM1", "Some desc"), true), print_r($item, true)));
            }

        }
    }


    /**
     * @return string
     */
    public function getListString(): string
    {
        $listedString = '';
        foreach ($this->list as $id => $data) {
            $listedString .= sprintf(' "%s" "%s"', $id, $data[0]);
        }
        return $listedString;
    }


    /**
     * Method is called to get the full command back
     * @return string
     * @throws \Exception
     */
    public function getArguments(): string
    {
        if (count($this->list) === 0) {
            throw new \Exception('The options list is empty');
        }
        return sprintf(
            "--%s %s \"%s\" %s %s %s %s",
            $this->name,
            $this->optionsToString(),
            $this->message,
            $this->height,
            $this->width,
            $this->height - 8,
            $this->getListString()
        );
    }

}