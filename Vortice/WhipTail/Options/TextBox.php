<?php
/**
 * @author    Philip Bergman <Vortice@live.nl>
 * @copyright Philip Bergman
 */

namespace Vortice\WhipTail\Options;

use Vortice\WhipTail\Helpers\WindowInterface;
use Vortice\WhipTail\Helpers\WindowTypes;

/**
 * Class TextBox
 *
 * A text box lets you display the contents of a text file in a dialog box. It is like a simple text file viewer.
 * The user can move through the file by using the UP/DOWN, PGUP/PGDN and HOME/END keys available on most keyboards.
 * If the lines are too long to be displayed in the box, the LEFT/RIGHT keys can be used to scroll the text region
 * horizontally. For more convenience, forward and backward searching functions are also provided.
 *
 * @package Vortice\WhipTail\Options
 */
class TextBox extends BaseOption implements WindowInterface
{
    public readonly string $file;

    public function __construct(int $width = 70, int $height = 20, string $message = '')
    {
        parent::__construct(
            name: WindowTypes::TextBox->value, width: $width, height: $height, message: $message
        );
    }

    /**
     * @throws \Exception
     */
    public function setFilename(string $filename)
    {
        if (!is_file($filename)) {
            throw new \Exception('Filename is not valid');
        }

        $this->file = $filename;
    }

    /**
     * Method is called to get the arguments
     *
     * @throws \Exception
     */
    public function getArguments()
    {
        if (empty($this->file) || (!is_file($this->file))) {
            throw new \Exception('No file defined');
        }

        return sprintf(
            "--%s %s \"%s\" %s %s",
            $this->name,
            $this->optionsToString(),
            $this->file,
            $this->height,
            $this->width
        );
    }
}