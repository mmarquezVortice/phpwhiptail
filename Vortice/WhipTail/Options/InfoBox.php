<?php
/**
 * @author    Philip Bergman <Vortice@live.nl>
 * @copyright Philip Bergman
 */

namespace Vortice\WhipTail\Options;

use Vortice\WhipTail\Helpers\WindowInterface;
use Vortice\WhipTail\Helpers\WindowTypes;

/**
 * Class InfoBox
 *
 * An info box is basically a message box. However, in this case,
 * whiptail will exit immediately after displaying the message to
 * the user. The screen is not cleared when whiptail exits, so
 * that the message will remain on the screen until the calling
 * shell script clears it later. This is useful when you want to
 * inform the user that some operations are carrying on that may
 * require some time to finish.
 *
 * @package Vortice\WhipTail\Options
 */
class InfoBox extends BaseOption implements WindowInterface
{
    public function __construct(int $width = 70, int $height = 20, string $message = '')
    {
        parent::__construct(
            name:   WindowTypes::InfoBox->value, width: $width, height: $height, message: $message
        );
    }
}