<?php
/**
 * @author    Philip Bergman <Vortice@live.nl>
 * @copyright Philip Bergman
 */

namespace Vortice\WhipTail\Options;

use Vortice\WhipTail\Helpers\WindowInterface;
use Vortice\WhipTail\Helpers\WindowTypes;

/**
 * Class PasswordBox
 *
 * A password box is similar to an input box, except the text the user enters is not displayed.
 * This is useful when prompting for passwords or other sensitive information. Be aware that if
 * anything is passed in "init", it will be visible in the system's process table to casual
 * snoopers. Also, it is very confusing to the user to provide them with a default password they
 * cannot see. For these reasons, using "init" is highly discouraged.
 *
 * @package Vortice\WhipTail\Options
 */
class PasswordBox extends BaseOption implements WindowInterface
{
    /** @var null  */
    protected $default = null;

    public function __construct(int $width = 70, int $height = 20, string $message = '')
    {
        parent::__construct(
          name: WindowTypes::PasswordBox->value, width: $width, height: $height, message: $message
        );
    }

    /**
     * Method is called to get the arguments
     */
    public function getArguments()
    {
        return sprintf(
            "--%s %s \"%s\" %s %s %s",
            $this->name,
            $this->optionsToString(),
            $this->message,
            $this->height,
            $this->width,
            $this->default
        );
    }


}