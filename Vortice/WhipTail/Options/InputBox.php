<?php
/**
 * @author    Philip Bergman <Vortice@live.nl>
 * @copyright Philip Bergman
 */

namespace Vortice\WhipTail\Options;

use Vortice\WhipTail\Helpers\WindowInterface;
use Vortice\WhipTail\Helpers\WindowTypes;

/**
 * Class InputBox
 *
 * An input box is useful when you want to ask questions that require the user
 * to input a string as the answer. If init is supplied it is used to initialize
 * the input string. When inputing the string, the BACKSPACE key can be used to
 * correct typing errors. If the input string is longer than the width of the
 * dialog box, the input field will be scrolled. On exit, the input string will
 * be printed on stderr.
 *
 * @package Vortice\WhipTail\Options
 */
class InputBox extends BaseOption implements WindowInterface
{
    /** @var null  */
    protected $default = null;

    public function __construct(int $width = 70, int $height = 20, string $message = '')
    {
        parent::__construct(
            name: WindowTypes::InputBox->value, width: $width, height: $height, message: $message
        );
    }

    /**
     * Method is called to get the arguments
     */
    public function getArguments()
    {
        return sprintf(
            "--%s %s \"%s\" %s %s %s",
            $this->name,
            $this->optionsToString(),
            $this->message,
            $this->height,
            $this->width,
            $this->default
        );
    }


}