<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Vortice\WhipTail\Helpers\BoxOptions;
use Vortice\WhipTail\Helpers\StatusOptions;
use Vortice\WhipTail\Helpers\WindowTypes;
use Vortice\WhipTail\WhipTail;

class RadioBoxTest extends TestCase
{
    private array $options = [
        ['ITEM1', "item 1", StatusOptions::ON],
        ['ITEM2', "item 2", StatusOptions::OFF],
        ['ITEM3', "item 3", StatusOptions::OFF],
        ['ITEM4', "item 4", StatusOptions::OFF],
        ['ITEM5', "item 5", StatusOptions::OFF],
        ['ITEM6', "item 6", StatusOptions::OFF],
        ['ITEM7', "item 7", StatusOptions::OFF],
        ['ITEM8', "item 8", StatusOptions::OFF],
    ];

    public function testShown()
    {
        if (WhipTail::isAvailable()) {
            $whipTail = new WhipTail(option: WindowTypes::RadioBox, message: 'Select item');
            /** @var \Vortice\WhipTail\Options\RadioBox $menuBox */
            $menuBox = $whipTail->box();
            $menuBox->setList($this->options);
            $whipTail
                ->option(BoxOptions::Title, 'Menu')
                ->run();

            $this->assertNotEmpty($whipTail->getResult());
        } else {
            throw new \Exception("Whiptail is not available");
        }
    }
}