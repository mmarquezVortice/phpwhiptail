<?php
declare(strict_types=1);

use Vortice\WhipTail\Helpers\BoxOptions;
use Vortice\WhipTail\Helpers\WindowTypes;
use Vortice\WhipTail\WhipTail;

class TextBoxTest extends \PHPUnit\Framework\TestCase
{
    private string $text = <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tincidunt semper nisl et pulvinar. Vestibulum vel ante vel sem rhoncus gravida. Donec ornare tristique felis, ac varius justo interdum quis. Duis iaculis eget nulla vel lobortis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec hendrerit erat non dui commodo, ut vehicula lectus hendrerit. Donec facilisis libero sapien, nec scelerisque dolor volutpat in. Cras molestie lorem in lectus venenatis mattis. Ut malesuada at elit nec blandit. Ut euismod, est sed ullamcorper luctus, ipsum arcu bibendum lorem, nec elementum lectus metus sit amet purus. Nam vel nulla purus. Pellentesque lacinia lacus ac ligula ultricies, non luctus massa ornare.

Duis viverra, sapien at porttitor tincidunt, ante risus porttitor eros, ac scelerisque dui leo vel orci. Suspendisse ac arcu sed metus semper faucibus blandit vel odio. Ut quis elit fermentum, sagittis metus eget, pretium elit. Vestibulum pulvinar nunc augue, a pretium orci rhoncus sit amet. Curabitur a odio et justo posuere tempor quis a urna. Nullam porta enim ac sem pretium euismod. Aenean est metus, porta eget diam quis, bibendum pulvinar nisi.

Nunc est ante, euismod id ultricies ac, fringilla dignissim tellus. Nullam id nulla sit amet leo egestas aliquam. Nunc consectetur, lorem in sagittis venenatis, magna eros gravida magna, et tempor felis lectus volutpat tortor. Donec nec accumsan tortor. Sed justo nibh, scelerisque id lacus blandit, fermentum malesuada mauris. Nunc at iaculis elit. Aliquam euismod ligula luctus dolor convallis pretium.

Sed bibendum condimentum sem. Sed venenatis in metus sit amet pharetra. In luctus, velit non blandit pellentesque, risus velit cursus urna, id dictum nunc quam eu lectus. Proin lacinia tempus enim, vitae luctus dolor hendrerit vel. Vivamus varius mi vitae eros posuere, nec aliquam lacus ornare. Curabitur vel condimentum nisi, sit amet porta libero. Nunc mollis sodales congue.

Proin ac elit vel sapien blandit consectetur. Curabitur placerat sagittis erat non consequat. Duis lectus felis, molestie in velit non, accumsan adipiscing dolor. Phasellus ut neque imperdiet, viverra neque ut, molestie tortor. Vestibulum sed porta lorem, eu blandit augue. Donec eget adipiscing arcu. In fringilla pharetra tellus, at pulvinar erat pretium vel.
EOF;

    public function testShown()
    {
        if (WhipTail::isAvailable()) {
            file_put_contents('./tmp.txt', $this->text);

            $whipTail = new WhipTail(option: WindowTypes::TextBox, message: $this->text);
            /** @var \Vortice\WhipTail\Options\TextBox $textBox */
            $textBox = $whipTail->box();
            $textBox->setFilename('./tmp.txt');
            $whipTail
                ->option(BoxOptions::Title, 'Text Box')
                ->option(BoxOptions::ScrollText, true)
                ->run();

            $this->assertTrue($whipTail->isTrue());
            unlink('./tmp.txt');
        } else {
            throw new \Exception("Whiptail is not available");
        }
    }
}