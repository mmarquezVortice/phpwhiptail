<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Vortice\WhipTail\Helpers\BoxOptions;
use Vortice\WhipTail\Helpers\WindowTypes;
use Vortice\WhipTail\WhipTail;

class MenuBoxTest extends TestCase
{
    private array $options = [
        ['ITEM1', "item 1"],
        ['ITEM2', "item 2"],
        ['ITEM3', "item 3"],
        ['ITEM4', "item 4"],
        ['ITEM5', "item 5"],
        ['ITEM6', "item 6"],
        ['ITEM7', "item 7"],
        ['ITEM8', "item 8"],
    ];

    public function testShown()
    {
        if (WhipTail::isAvailable()) {
            $whipTail = new WhipTail(option: WindowTypes::MenuBox, message: 'Select item');
            /** @var \Vortice\WhipTail\Options\MenuBox $menuBox */
            $menuBox = $whipTail->box();
            $menuBox->setList($this->options);
            $whipTail
                ->option(BoxOptions::Title, 'Menu')
                ->run();

            $this->assertNotEmpty($whipTail->getResult());
        } else {
            throw new \Exception("Whiptail is not available");
        }
    }
}