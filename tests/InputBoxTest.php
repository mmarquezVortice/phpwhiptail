<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Vortice\WhipTail\Helpers\BoxOptions;
use Vortice\WhipTail\Helpers\WindowTypes;
use Vortice\WhipTail\WhipTail;

class InputBoxTest extends TestCase
{
    public function testShown()
    {
        if (WhipTail::isAvailable()) {
            $whipTail = new WhipTail(option: WindowTypes::InputBox, height: 10, message: 'Some Input');
            $whipTail
                ->option(BoxOptions::Title, 'Input Box')
                ->run();

            $this->assertNotEmpty($whipTail->getResult());
        } else {
            throw new \Exception("Whiptail is not available");
        }
    }
}