<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Vortice\WhipTail\Helpers\BoxOptions;
use Vortice\WhipTail\Helpers\StatusOptions;
use Vortice\WhipTail\Helpers\WindowTypes;
use Vortice\WhipTail\WhipTail;

class CheckBoxTest extends TestCase
{
    private array $options = [
        ['ITEM1', "item 1", StatusOptions::ON],
        ['ITEM2', "item 2", StatusOptions::OFF],
        ['ITEM3', "item 3", StatusOptions::OFF],
        ['ITEM4', "item 4", StatusOptions::OFF],
        ['ITEM5', "item 5", StatusOptions::OFF],
        ['ITEM6', "item 6", StatusOptions::OFF],
        ['ITEM7', "item 7", StatusOptions::OFF],
        ['ITEM8', "item 8", StatusOptions::OFF],
    ];

    /**
     * Checkbox result should be an array with the selected options
     * @throws Exception
     */
    public function testShown()
    {
        if (WhipTail::isAvailable()) {
            $whipTail = new WhipTail(option: WindowTypes::CheckBox, message: 'Select item(s) (one at least to pass test)');
            /** @var \Vortice\WhipTail\Options\CheckBox $menuBox */
            $menuBox = $whipTail->box();
            $menuBox->setList($this->options);
            $whipTail
                ->option(BoxOptions::Title, 'CheckList')
                ->run();

            $result = $whipTail->getResult();
            $this->assertNotEmpty($result);
        } else {
            throw new \Exception("Whiptail is not available");
        }
    }
}