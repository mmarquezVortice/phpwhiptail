<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Vortice\WhipTail\Helpers\BoxOptions;
use Vortice\WhipTail\Helpers\WindowTypes;
use Vortice\WhipTail\WhipTail;

class DemoInfoBoxTest extends TestCase
{
    public function testShown()
    {
        if (WhipTail::isAvailable()) {
            $whipTail = new WhipTail(option: WindowTypes::InfoBox, message: 'Some info');
            $whipTail
                ->option(BoxOptions::Title, 'Info Box')
                ->run();

            $this->addToAssertionCount(1);
        } else {
            throw new \Exception("Whiptail is not available");
        }
    }
}