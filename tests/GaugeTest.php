<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Vortice\WhipTail\Helpers\BoxOptions;
use Vortice\WhipTail\Helpers\WindowTypes;
use Vortice\WhipTail\Options\GaugeBox;
use Vortice\WhipTail\WhipTail;

class GaugeTest extends TestCase
{
    public function testShown()
    {
        if (WhipTail::isAvailable()) {
            $whipTail = new WhipTail(option: WindowTypes::GaugeBox, message: 'Starting....');
            $whipTail->option(BoxOptions::Title, 'Testing gauge');
            /** @var GaugeBox $gauge */
            $gauge = $whipTail->box();
            $gauge
                ->addCallBack(function(){ sleep(1);}, [], 'Testing')
                ->addCallBack(function(Vortice\WhipTail\Helpers\Progress $progress, $parts){

                    $progress->setParts($parts);

                    for($i = 0; $i < $parts; $i++){
                        sleep(1);
                        $progress->advance('Starting part ' . $i);
                    }

                }, [3], 'Testing 2' )
                ->addCallBack(function(){ sleep(1);}, array(), 'Testing 3');
            $whipTail
                ->run();

            $this->addToAssertionCount(1);
        } else {
            throw new \Exception("Whiptail is not available");
        }
    }
}