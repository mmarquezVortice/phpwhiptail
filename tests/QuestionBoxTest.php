<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Vortice\WhipTail\Helpers\BoxOptions;
use Vortice\WhipTail\Helpers\WindowTypes;
use Vortice\WhipTail\WhipTail;

class QuestionBoxTest extends TestCase
{
    public function testShown()
    {
        if (WhipTail::isAvailable()) {
            $whipTail = new WhipTail(option: WindowTypes::QuestionBox, message: 'Is it working? (Yes to pass test)');
            $whipTail
                ->option(BoxOptions::Title, 'Yes or No')
                ->run();

            $this->assertTrue($whipTail->isTrue());
        } else {
            throw new \Exception("Whiptail is not available");
        }
    }
}