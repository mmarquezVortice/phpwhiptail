<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Vortice\WhipTail\Helpers\BoxOptions;
use Vortice\WhipTail\Helpers\WindowTypes;
use Vortice\WhipTail\WhipTail;

class MessageBoxTest extends TestCase
{
    public function testShown()
    {
        if (WhipTail::isAvailable()) {
            $whipTail = new WhipTail(option: WindowTypes::MessageBox, height: 7, message: 'This is a message');
            $whipTail
                ->option(BoxOptions::Title, 'Message')
                ->run();

            $this->assertEmpty($whipTail->getResult());
        } else {
            throw new \Exception("Whiptail is not available");
        }
    }
}