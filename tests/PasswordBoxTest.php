<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Vortice\WhipTail\Helpers\BoxOptions;
use Vortice\WhipTail\Helpers\WindowTypes;
use Vortice\WhipTail\WhipTail;

class PasswordBoxTest extends TestCase
{
    public function testShown()
    {
        if (WhipTail::isAvailable()) {
            $whipTail = new WhipTail(option: WindowTypes::PasswordBox, message: 'new password (set not empty to pass test)');
            $whipTail
                ->option(BoxOptions::Title, 'Password')
                ->run();

            $this->assertNotEmpty($whipTail->getResult());
        } else {
            throw new \Exception("Whiptail is not available");
        }
    }
}